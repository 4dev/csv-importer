var myapp = angular.module('filecontrol', []);

myapp.controller('FileController', function ($scope) {
    $scope.inputfile = {};
    $scope.columns;
        
    this.readInFile = function($fileContent) {
        $scope.inputfile.csvobjects = $.csv.toObjects($fileContent);
        $scope.columns = 100 / ($scope.inputfile.csvobjects[0].length) + '%';
    };
});

myapp.directive('makeDraggable', function() {
    return function(scope, element) {

        if (scope.$index == 0) {
            element.attr('data-val', scope.value);
        }

        if (scope.$last){
            makeDraggable();
        }
    };
});

myapp.directive('statement', function() {
    return {
        restrict: 'E',
        templateUrl: "html/statement.html"
    }
});

/*
* Handles the file when uploaded
*/
myapp.directive('onReadFile', function ($parse) {
    return {
        restrict: 'A',
        scope: false,
        link: function(scope, element, attrs) {
            var fn = $parse(attrs.onReadFile);

            element.on('change', function(onChangeEvent) {
                var reader = new FileReader();

                reader.onload = function(onLoadEvent) {
                    scope.$apply(function() {
                        fn(scope, {$fileContent:onLoadEvent.target.result});
                    });
                };

                reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
            });
        }
    };
});