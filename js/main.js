$(init);

function init() {}

function addNumbers(num1, num2) {
    return Math.round((num1 + num2) * 1e2) / 1e2;
}

function makeDroppable() {
    $('.catch_note_item').each(function(e) {
        var sib = $(this).children('.remove_button')[0];
        $(this).hover(
            function () {
                $(sib).fadeIn('slow');
            },
            function () {
                $(sib).fadeOut('slow');
            }
        );
    });

    $('.droppable').droppable({
        hoverClass: 'begin_drop',
        drop: function (event, ui) {
            var localScope = $( event.target ).scope();
            var draggable = ui.draggable;
            var slotNumber = $(this).attr('data-val');
            var cardNumber = ui.draggable.find('[data-val]').attr('data-val');
            if (!slotNumber) {
                $(this).attr('data-val', cardNumber);
                // Invoke the expression in the local scope
                // context to make sure we adhere to the
                // proper scope chain prototypal inheritance.
                localScope.$apply(
                    function() {
                        localScope.$parent.categories[localScope.$index].amount = Number(cardNumber).toFixed(2);
                    }
                );
            } else {
                var total = addNumbers(Number(slotNumber), Number(cardNumber));
                $(this).attr('data-val', total);
                localScope.$apply(
                    function() {
                        localScope.$parent.categories[localScope.$index].amount = Number(total.toFixed(2));
                    }
                );
            }
            draggable.hide();
        }
    });

    $(".fancybox").fancybox({
        closeBtn	: true
    });
}

function makeDraggable() {
    $('.draggable').draggable({
        revert: true,
        cursor: "move",
        cursorAt: { top: 10, left: 55},
        helper: function( event ) {
            return $( "<div style='width:200px;' class='ui-widget-header'>" + event.target.textContent + "</div>" );
        }
    });
}