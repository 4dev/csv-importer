var categoryctrl = angular.module('categorycontrol', []);

categoryctrl.service('CategoryService', [ '$rootScope', function($rootScope) {

    return {
        categories: [],
        addCategory: function (val) {
            this.categories.push({
                label: val,
                amount: ''
            })
        },
        amountTotal: function() {
            var total = 0;
            for(var count = 0; count < this.categories.length; count++){
                total += Number(this.categories[count].amount);
            }
            return total.toFixed(2);
        },
        removeCategory: function(index) {
            this.categories.splice(index,1);
        }
    }
}]);

categoryctrl.controller('CategoryController', ['CategoryService', '$scope', function(CategoryService, $scope){

    $scope.categories = CategoryService.categories;

    $scope.amountTotal = function() {
        return CategoryService.amountTotal();
    };

    $scope.addCategory = function() {
        CategoryService.addCategory($scope.categoryLabel);
        $scope.categoryLabel = '';
    };

    $scope.removeCategory = function(index) {
        CategoryService.removeCategory(index);
    };
}]);

categoryctrl.directive('makeDropable', function() {
    return function(scope, element) {
        if (scope.$last){
            makeDroppable();
        }
    };
});

categoryctrl.directive('updateAmount', function() {
    return function(scope, element) {
        scope.amountHTML = element.html();
    };
});

categoryctrl.directive('categories', function() {
    return {
        restrict: 'E',
        templateUrl: "html/categories.html"
    }
});
